﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Forum.DataModels
{
    public class ForumTopicEntry
    {
        [Key]
        public int TopicId { get; set; }

        [StringLength(Models.ForumTopic.MAX_ID_LENGTH)]
        [Index(IsUnique=true)]
        public string Id { get; set; }

        [StringLength(Models.ForumTopic.MAX_NAME_LENGTH)]
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ForumTopicEntry ParentTopic { get; set; }

        public virtual ICollection<ForumTopicEntry> ChildTopics { get; set; }

        public ForumTopicEntry()
        {
            ChildTopics = new List<ForumTopicEntry>();
        }
    }

    public class ForumThreadEntry
    {
        [Key]
        public int ThreadId { get; set; }
        [StringLength(Models.ForumThread.MAX_TITLE_LENGTH)]
        public string Title { get; set; }
 
        [Index]
        public int TopicId { get; set; }
        [ForeignKey("TopicId")]
        public ForumTopicEntry Topic { get; set; }

        public string AuthorId { get; set; }
        [ForeignKey("AuthorId")]
        public ApplicationUser Author { get; set; }
    }

    public class ForumReplyEntry
    {
        [Key]
        public int ReplyId { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public ApplicationUser Author { get; set; }
        [Index]
        public int ThreadId { get; set; }
        [ForeignKey("ThreadId")]
        public ForumThreadEntry Thread { get; set; }
        public DateTime Date { get; set; }
    }
}