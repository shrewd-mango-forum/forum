﻿using System;
using System.Web.Configuration;

namespace Forum.Managers
{
    public class ForumConfiguration
    {
        public static ForumConfiguration config = new ForumConfiguration();

        public Boolean DropCreateAlways {
            get {
                string initializerSetting = WebConfigurationManager.AppSettings["DbInitializer"];
                return "DropCreateDatabaseAlways".Equals(initializerSetting);
            }
        }

        public Boolean Demo
        {
            get
            {
                return "true".Equals(WebConfigurationManager.AppSettings["Demo"]?.ToLower());
            }
        }
    }
}