﻿using Forum.DataModels;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Managers
{
    internal class ReplyManager : IDisposable
    {
        readonly ApplicationDbContext db;
        private ReplyManager(ApplicationDbContext db)
        {
            this.db = db;
        }
        public void Dispose()
        {
        }

        internal async Task<IEnumerable<ForumReplyEntry>> GetReplies(int threadId)
        {
            await db.Threads.SingleAsync(thread => thread.ThreadId.Equals(threadId));
            return await db.Replies
                .Where(re => re.ThreadId.Equals(threadId))
                .Include(re => re.Author)
                .ToListAsync();
        }

        internal async Task AddReply(ForumReplyEntry entry)
        {
            if (entry.Thread == null)
            {
                throw new ArgumentException($"Expected {nameof(entry)}.{nameof(entry.Thread)} to be non-null.");
            }
            db.Replies.Add(entry);
            await db.SaveChangesAsync();
        }

        internal static ReplyManager Create(IdentityFactoryOptions<ReplyManager> options, IOwinContext context)
        {
            return new ReplyManager(context.Get<ApplicationDbContext>());
        }
    }
}