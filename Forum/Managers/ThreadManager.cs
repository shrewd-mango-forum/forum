﻿using Forum.DataModels;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Managers
{
    internal class ThreadManager : IDisposable
    {
        readonly ApplicationDbContext db;
        private ThreadManager(ApplicationDbContext db)
        {
            this.db = db;
        }
        public void Dispose()
        {
        }

        internal async Task<ForumThreadEntry> GetThread(int threadId)
        {
            return await db.Threads
                .Where(th => th.ThreadId.Equals(threadId))
                .Include(th => th.Author)
                .Include(th => th.Topic)
                .AsQueryable()
                .SingleOrDefaultAsync();
        }

        internal async Task<bool> ThreadExists(int threadId)
        {
            return await db.Threads
                .Where(th => th.ThreadId.Equals(threadId))
                .AnyAsync();
        }

        internal async Task<ForumThreadEntry> CreateThread(ForumThreadEntry thread, ForumReplyEntry firstReply)
        {
            db.Threads.Add(thread);
            firstReply.Thread = thread;
            db.Replies.Add(firstReply);
            await db.SaveChangesAsync();
            return thread;
        }

        internal async Task DeleteThread(int threadId)
        {
            var thread = await GetThread(threadId);
            if (thread != null)
            {
                db.Threads.Remove(thread);
                await db.SaveChangesAsync();
            }
        }

        internal static ThreadManager Create(IdentityFactoryOptions<ThreadManager> options, IOwinContext context)
        {
            return new ThreadManager(context.Get<ApplicationDbContext>());
        }
    }
}