﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Web.Configuration;

using Forum.Managers;

namespace Forum.DataModels {
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {
		public ApplicationDbContext()
			: base("DefaultConnection", throwIfV1Schema: false) {
		}

		static ApplicationDbContext() {
            var initializer = ForumConfiguration.config.DropCreateAlways
                    ? (IDatabaseInitializer<ApplicationDbContext>) new DropCreateAlways()
                   : new DropCreateIfModelChanges();

            Database.SetInitializer(initializer);
		}

		public static ApplicationDbContext Create() {
			return new ApplicationDbContext();
		}

        public DbSet<ForumTopicEntry> Topics { get; set; }
        public DbSet<ForumThreadEntry> Threads { get; set; }
        public DbSet<ForumReplyEntry> Replies { get; set; }

		public class DropCreateAlways : DropCreateDatabaseAlways<ApplicationDbContext> {
			protected override void Seed(ApplicationDbContext context) {
				PerformInitialSetup(context);
				base.Seed(context);
			}
		}

        public class DropCreateIfModelChanges : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
        {
            protected override void Seed(ApplicationDbContext context)
            {
                PerformInitialSetup(context);
                base.Seed(context);
            }
        }

        static private void PerformInitialSetup(ApplicationDbContext context)
        {
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            string userName = "Admin";
            string userPass = "MySecret";
            ApplicationUser admin = new ApplicationUser { UserName = userName };
            userManager.Create(admin, userPass);

            RoleManager<ApplicationRole> roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(context));
            ApplicationRole adminRole = new ApplicationRole("Administrator");
            roleManager.Create(adminRole);
            userManager.AddToRole(admin.Id, adminRole.Name);
        }
    }
}