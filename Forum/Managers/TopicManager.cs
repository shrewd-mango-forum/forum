﻿using Forum.DataModels;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.Managers
{
    public class TopicManager : IDisposable
    {
        readonly ApplicationDbContext db;
        private TopicManager(ApplicationDbContext db)
        {
            this.db = db;
        }
        public void Dispose()
        {
        }

        public async Task<List<ForumTopicEntry>> ListRoot()
        {
            return await db.Topics.Where(t => t.ParentTopic.Equals(null)).ToListAsync();
        }

        public async Task<ForumTopicEntry> GetTopic(string topicId)
        {
            return await db.Topics
                .Where(to => to.Id.Equals(topicId))
                .SingleOrDefaultAsync();
        }

        public async Task<ForumTopicEntry> GetTopicDetails(string topicId)
        {
            return await db.Topics
                .Where(to => to.Id.Equals(topicId))
                .Include(to => to.ChildTopics)
                .SingleOrDefaultAsync();
        }

        public async Task<bool> TopicExists(string topicId)
        {
            return await db.Topics
                .Where(to => to.Id.Equals(topicId))
                .AnyAsync();
        }

        public async Task<List<ForumThreadEntry>> GetThreadsUnderTopic(int topicId)
        {
            return await db.Threads
                .Where(th => th.TopicId.Equals(topicId))
                .ToListAsync();
        }

        public async Task<ForumTopicEntry> CreateTopic(ForumTopicEntry topic)
        {
            db.Topics.Add(topic);
            await db.SaveChangesAsync();
            return topic;
        }

        public async Task<ForumTopicEntry> CreateTopic(ForumTopicEntry topic, string parentTopicId)
        {
            var parent = await db.Topics.Where(t => t.Id.Equals(parentTopicId)).Include(t => t.ChildTopics).SingleAsync();
            parent.ChildTopics.Add(topic);
            topic.ParentTopic = parent;
            db.Topics.Add(topic);
            await db.SaveChangesAsync();
            return topic;
        }

        public async Task DeleteTopicAndChildren(string topicId)
        {
            var topic = await db.Topics
                .Where(t => t.Id.Equals(topicId))
                .FirstOrDefaultAsync();
            if (topic != null)
            {
                await DeleteTopic(topic.TopicId);
            }
            await db.SaveChangesAsync();
        }

        private async Task DeleteTopic(int topicId)
        {
            var topic = await db.Topics
                .Where(t => t.TopicId.Equals(topicId))
                .Include(t => t.ChildTopics)
                .FirstAsync();
            await Task.WhenAll(
                topic.ChildTopics.Select(async t => await DeleteTopic(t.TopicId))
            );
            db.Topics.Remove(topic);
        }

        public static TopicManager Create(IdentityFactoryOptions<TopicManager> options, IOwinContext context)
        {
            return new TopicManager(context.Get<ApplicationDbContext>());
        }
    }
}