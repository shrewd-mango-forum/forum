﻿using Microsoft.Owin;
using Owin;
using System.Web.Mvc;
using System.Web.Http;

[assembly: OwinStartup(typeof(Forum.Startup))]

namespace Forum
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            WebApiConfig.Register(config);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            ConfigureAuth(app);
            app.UseWebApi(config);
        }
    }
}
