﻿using Forum.Filters;
using System;
using System.Web;

namespace Forum {
	public class WebApiApplication : HttpApplication {
        private CorsWithIEWorkaroundFilter corsWorkaround = new CorsWithIEWorkaroundFilter();

		protected void Application_Start() {
		}

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var context = sender.GetType().GetProperty("Context")?.GetValue(sender) as HttpContext;
            if (context != null)
            {
                corsWorkaround.execute(context);
            }
        }
	}
}
