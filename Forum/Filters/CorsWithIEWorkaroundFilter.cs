﻿using System;
using System.Net;
using System.Web;
using System.Web.Cors;

namespace Forum.Filters
{
    /**
     * Workaround for an IE/Edge issue where the browser expects 'accept' under AccessControlAllowHeaders.
     */
    public class CorsWithIEWorkaroundFilter
    {
        public void execute(HttpContext context)
        {
            HttpRequest request = context.Request;
            if (isCorsPreflight(request))
            {
                var response = context.Response;
                context.Response.StatusCode = (int) HttpStatusCode.OK;
                response.Headers.Add(CorsConstants.AccessControlAllowMethods, request.Headers[CorsConstants.AccessControlRequestMethod]);
                response.Headers.Add(CorsConstants.AccessControlAllowOrigin, "*");
                var requestHeaders = request.Headers.GetValues(CorsConstants.AccessControlRequestHeaders);
                if (requestHeaders != null)
                {
                    response.Headers.Add(CorsConstants.AccessControlAllowHeaders, String.Join(",", requestHeaders));
                }

                response.End();
            }
        }

        private bool isCorsPreflight(HttpRequest request)
        {
            return
                request.Headers[CorsConstants.AccessControlRequestMethod] != null
                && string.Equals(request.HttpMethod, CorsConstants.PreflightHttpMethod, StringComparison.OrdinalIgnoreCase);
        }
    }
}