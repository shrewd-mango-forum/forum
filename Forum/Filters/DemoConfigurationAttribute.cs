﻿using Forum.Managers;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Forum.Filters
{
    public class DemoConfigurationAttribute: ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!ForumConfiguration.config.Demo)
            {
                var response = actionContext.Request.CreateErrorResponse(
                    HttpStatusCode.NotFound, "This instance is not configured to allow demo actions.");
                throw new HttpResponseException(response);
            }
        }
    }
}