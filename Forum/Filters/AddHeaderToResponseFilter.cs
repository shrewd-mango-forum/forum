﻿using System.Web.Http.Filters;

namespace Forum.Filters
{
    public class AddHeaderToResponseFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response?.Headers != null)
            {
                //(IE loves to cache)
                actionExecutedContext.Response.Headers.Add("Cache-Control", "no-cache");
            }
        }
    }
}