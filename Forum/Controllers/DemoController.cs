﻿using Forum.Filters;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Forum.Controllers
{
    [RoutePrefix("api/demo")]
    [DemoConfiguration]
    public class DemoController : ApiController
    {
        private ApplicationUserManager UserManager
        {
            get
            {
                return Request.GetOwinContext().Get<ApplicationUserManager>();
            }
        }

        [Route]
        [HttpHead]
        public IHttpActionResult Head()
        {
            return Ok();
        }

        [Route("trustMe")]
        [Authorize]
        [HttpPost]
        public async Task<IHttpActionResult> ForceAdmin()
        {
            string userId = User.Identity.GetUserId();
            await UserManager.AddToRoleAsync(userId, "Administrator");
            return Ok();
        }
    }
}
