﻿using Forum.Controllers;
using Forum.DataModels;
using Forum.Managers;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using static Forum.Models.GetTopicResponseModel;

namespace Forum.Models
{
    [RoutePrefix("api/Topic")]
    public class TopicController : ForumApiController
    {
        public const string GET_TOPIC_ROUTE = "getTopic";
        private const string deleteTopicRoute = "deleteTopic";
        private const string getRootTopicRoute = "getRootTopic";
        private const string postRootTopicRoute = "postRootTopic";
        private const string postTopicRoute = "postTopic";
        private const string postThreadRoute = "postThread";

        private TopicManager TopicManager
        {
            get
            {
                return Request.GetOwinContext().Get<TopicManager>();
            }
        }

        // GET: /
        // GET: api/Topic
        [HttpGet]
        [ResponseType(typeof(GetTopicResponseModel))]
        [Route("~/")]
        [Route("", Name = getRootTopicRoute)]
        public async Task<GetTopicResponseModel> ListTopics()
        {
            List<ForumTopicEntry> topics = await TopicManager.ListRoot();

            bool allowNewTopic = User.IsInRole("Administrator");
            GetTopicResponseModel response = new GetTopicResponseModel
            {
                Topics = topics.Select(toTopicSummary),
                Actions = new TopicActions
                {
                    GetTopicUrl = Url.Link(getRootTopicRoute, new { }),
                    PostTopicUrl = allowNewTopic ? Url.Link(postRootTopicRoute, new { }) : null
                },
                Threads = Enumerable.Empty<ThreadSummary>()
            };
            return response;
        }

        // GET: api/Topic/topicId
        [HttpGet]
        [ResponseType(typeof(GetTopicResponseModel))]
        [Route("{id}", Name = GET_TOPIC_ROUTE)]
        public async Task<GetTopicResponseModel> GetTopic(string id)
        {
            ForumTopicEntry topic = await TopicManager.GetTopicDetails(id);
            if (topic == null)
            {
                throw new HttpResponseException(System.Net.HttpStatusCode.NotFound);
            }
            IEnumerable<ForumThreadEntry> threads = await TopicManager.GetThreadsUnderTopic(topic.TopicId);

            TopicSummary parent = topic.ParentTopic != null
                ? toTopicSummary(topic.ParentTopic)
                : new TopicSummary {
                    Id = null,
                    GetTopicUrl = Url.Link(getRootTopicRoute, new { }),
                    Description = null
                  };

            GetTopicResponseModel response = new GetTopicResponseModel
            {
                Id = topic.Id,
                Name = topic.Name,
                Description = topic.Description,
                Topics = topic.ChildTopics.Select(toTopicSummary),
                Parent = parent,
                Actions = topicActions(topic.Id)
            };
            response.Threads = threads.Select(th => new ThreadSummary {
                Title = th.Title,
                Id = th.ThreadId.ToString(),
                ThreadUrl = Url.Link(ThreadController.GET_THREAD_ROUTE, new { threadId = th.ThreadId })
            });
            return response;
        }

        // POST: api/Topic
        [HttpPost]
        [Authorize(Roles="Administrator")]
        [ResponseType(typeof(PostTopicResponseModel))]
        [Route("", Name = postRootTopicRoute)]
        public async Task<IHttpActionResult> PostTopic([FromBody] PostTopicRequestModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ForumTopicEntry topic = new ForumTopicEntry {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description
            };
            if (await TopicManager.TopicExists(topic.Id)) {
                return Conflict();
            }

            await TopicManager.CreateTopic(topic);

            string getTopicUrl = Url.Link(GET_TOPIC_ROUTE, new { id = topic.Id });
            return Created(getTopicUrl, new PostTopicResponseModel { Actions = topicActions(topic.Id) });
        }

        // POST: api/Topic/topicName
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ResponseType(typeof(PostTopicResponseModel))]
        [Route("{id}", Name = postTopicRoute)]
        public async Task<IHttpActionResult> PostChildTopic(string id, [FromBody] PostTopicRequestModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (! await TopicManager.TopicExists(id))
            {
                return NotFound();
            }

            ForumTopicEntry topic = new ForumTopicEntry {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description
            };
            if (await TopicManager.TopicExists(topic.Id))
            {
                return Conflict();
            }

            await TopicManager.CreateTopic(topic, id);
            string getTopicUrl = Url.Link(GET_TOPIC_ROUTE, new { id = topic.Id });
            return Created(getTopicUrl, new PostTopicResponseModel { Actions = topicActions(topic.Id) });
        }

        // DELETE: api/Topic/topicName
        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        [Route("{id}", Name = deleteTopicRoute)]
        public async Task<IHttpActionResult> DeleteTopic(string id)
        {
            await TopicManager.DeleteTopicAndChildren(id);
            return Ok();
        }

        private TopicActions topicActions(string topicId)
        {
            bool allowNewThread = User.Identity.IsAuthenticated;
            bool topicAdmin = User.IsInRole("Administrator");

            return new TopicActions
            {
                DeleteTopicUrl = topicAdmin ? Url.Link(deleteTopicRoute, new { id = topicId }) : null, 
                GetTopicUrl = Url.Link(GET_TOPIC_ROUTE, new { id = topicId }),
                PostThreadUrl = allowNewThread ? Url.Link(ThreadController.POST_THREAD_ROUTE, new { }) : null,
                PostTopicUrl = topicAdmin ? Url.Link(postTopicRoute, new { id = topicId }) : null
            };
        }
    }
}