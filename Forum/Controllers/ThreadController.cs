﻿using Forum.DataModels;
using Forum.Managers;
using Forum.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Forum.Controllers
{
    [RoutePrefix("api/Thread")]
    public class ThreadController : ForumApiController
    {
        public const string GET_THREAD_ROUTE = "getThread";
        public const string DELETE_THREAD_ROUTE = "deleteThread";
        public const string POST_THREAD_ROUTE = "postThread";
        public const string REPLY_ROUTE = "replyToThread";
        private TopicManager TopicManager
        {
            get
            {
                return Request.GetOwinContext().Get<TopicManager>();
            }
        }
        private ThreadManager ThreadManager
        {
            get
            {
                return Request.GetOwinContext().Get<ThreadManager>();
            }
        }
        private ReplyManager ReplyManager
        {
            get
            {
                return Request.GetOwinContext().Get<ReplyManager>();
            }
        }
                
        private ApplicationUserManager UserManager
        {
            get
            {
                return Request.GetOwinContext().Get<ApplicationUserManager>();
            }
        }

        [ResponseType(typeof(GetThreadResponseModel))]
        [Route("{threadId}", Name = GET_THREAD_ROUTE)]
        public async Task<IHttpActionResult> GetThread(int threadId)
        {
            var thread = await ThreadManager.GetThread(threadId);
            if (thread == null)
            {
                return NotFound();
            }
            var replies = await ReplyManager.GetReplies(threadId);
            var response = new GetThreadResponseModel
            {
                Title = thread.Title,
                Id = thread.ThreadId.ToString(),
                Topic = toTopicSummary(thread.Topic),
                Author = thread.Author.UserName,
                Replies = replies.Select(r => new GetThreadResponseModel.ThreadReply
                {
                    Content = r.Content,
                    Author = r.Author.UserName,
                    Date = r.Date
                }).ToList(),
                Actions = ThreadActions(thread.ThreadId)
            };
            return Ok(response);
        }

        [Authorize]
        [ResponseType(typeof(PostThreadResponseModel))]
        [Route("", Name = POST_THREAD_ROUTE)]
        public async Task<IHttpActionResult> PostThread([FromBody] PostThreadRequestModel post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ForumTopicEntry parentTopic = await TopicManager.GetTopic(post.ParentTopicId);
            ApplicationUser author = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            ForumThreadEntry thread = new ForumThreadEntry
            {
                Title = post.Title,
                Topic = parentTopic,
                Author = author
            };

            ForumReplyEntry firstReply = new ForumReplyEntry { Author = author, Date = DateTime.UtcNow, Content = post.Content };
            await ThreadManager.CreateThread(thread, firstReply);

            return CreatedAtRoute(
                GET_THREAD_ROUTE,
                new { threadId = thread.ThreadId},
                new PostThreadResponseModel {
                    Id = thread.ThreadId.ToString(),
                    Actions = ThreadActions(thread.ThreadId) }
            );
        }

        [Authorize]
        [ResponseType(typeof(int))]
        [Route("{threadId}/Reply", Name = REPLY_ROUTE)]
        public async Task<IHttpActionResult> PostReply(int threadId, [FromBody] PostReplyRequestModel replyRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ForumThreadEntry thread = await ThreadManager.GetThread(threadId);
            if (thread == null)
            {
                return NotFound();
            }

            ApplicationUser author = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            DateTime date = DateTime.UtcNow;
            ForumReplyEntry reply = new ForumReplyEntry { Thread = thread, Author = author, Content = replyRequest.Content, Date = date };

            await ReplyManager.AddReply(reply);

            return CreatedAtRoute(GET_THREAD_ROUTE, new { threadId = thread.ThreadId }, reply.ReplyId);
        }

        [Authorize(Roles = "Administrator")]
        [Route("{threadId}", Name = DELETE_THREAD_ROUTE)]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteThread(int threadId)
        {
            await ThreadManager.DeleteThread(threadId);
            return Ok();
        }

        public ThreadActions ThreadActions(int threadId)
        {
            bool allowDelete = User.IsInRole("Administrator");
            bool allowReply = User.Identity.IsAuthenticated;
            return new ThreadActions
            {
                GetUrl = Url.Link(GET_THREAD_ROUTE, new { threadId = threadId }),
                ReplyUrl = allowReply ? Url.Link(REPLY_ROUTE, new { threadId = threadId }) : null,
                DeleteUrl = allowDelete ? Url.Link(DELETE_THREAD_ROUTE, new { threadId = threadId }) : null
            };
        }
    }
}
