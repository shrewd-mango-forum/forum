﻿using Forum.DataModels;
using Forum.Models;
using System.Web.Http;

namespace Forum.Controllers
{
    public abstract class ForumApiController: ApiController
    {        
        protected TopicSummary toTopicSummary(ForumTopicEntry topicEntry)
        {
            return new TopicSummary
            {
                Id = topicEntry.Id,
                Name = topicEntry.Name,
                Description = topicEntry.Description,
                GetTopicUrl = Url.Link(TopicController.GET_TOPIC_ROUTE, new { id = topicEntry.Id })
            };
        }
    }
}
