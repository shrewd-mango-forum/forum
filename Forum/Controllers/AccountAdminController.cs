﻿using Forum.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Forum.Controllers
{
    [RoutePrefix("api/AccountAdmin")]
    [Authorize(Roles="Administrator")]
    public class AccountAdminController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountAdminController()
        {
        }

        public AccountAdminController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // Get api/AccountAdmin/User/
        [HttpGet]
        [Route("User")]
        public GetUsersModel ListUsers()
        {
            return new GetUsersModel {
                Users = UserManager.Users.Select(u => new ForumUser { UserName = u.UserName }).ToList()
            };
        }

        // DELETE api/AccountAdmin/User/userName
        [Route("User/{userName}")]
        public async Task DeleteUser(string userName)
        {
            await UserManager.DeleteAsync(await (UserManager.FindByNameAsync(userName)));
        }
    }
}