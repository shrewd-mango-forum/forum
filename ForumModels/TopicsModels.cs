﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class GetTopicResponseModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TopicSummary Parent { get; set; }
        public IEnumerable<ThreadSummary> Threads { get; set; }
        //subtopics
        public IEnumerable<TopicSummary> Topics { get; set; }
        public TopicActions Actions { get; set; }

        public class ThreadSummary
        {
            public string ThreadUrl { get; set; }
            public string Title { get; set; }
            public string Id { get; set; }
        }
    }

    public class PostTopicRequestModel
    {
        [Required]
        [StringLength(ForumTopic.MAX_ID_LENGTH)]
        [RegularExpression(@"^[a-zA-Z0-9\-\.)]+$", ErrorMessage = "Only English letters, numerals, '-', and '.' are allowed.")]
        public string Id { get; set; }
        [Required]
        [StringLength(ForumTopic.MAX_NAME_LENGTH)]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
    }

    public class PostTopicResponseModel
    {
        public TopicActions Actions { get; set; }
    }

    public class ForumTopic
    {
        public const int MAX_ID_LENGTH = 255;
        public const int MAX_NAME_LENGTH = 255;
    }

    public class TopicActions
    {
        public string DeleteTopicUrl { get; set; }
        public string GetTopicUrl { get; set; }
        public string PostTopicUrl { get; set; }
        public string PostThreadUrl { get; set; }
    }

    public class TopicSummary
    {
        public string GetTopicUrl { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}