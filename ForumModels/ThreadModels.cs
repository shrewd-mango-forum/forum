﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class PostThreadRequestModel
    {
        [Required]
        public string ParentTopicId { get; set; }
        [StringLength(ForumThread.MAX_TITLE_LENGTH)]
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
    }

    public class PostThreadResponseModel
    {
        public string Id { get; set; }
        public ThreadActions Actions { get; set; }
    }

    public class GetThreadResponseModel
    {
        public string Title { get; set; }
        public string Id { get; set; }
        public TopicSummary Topic { get; set; }
        public string Author { get; set; }
        public List<ThreadReply> Replies { get; set; }
        public ThreadActions Actions { get; set; }

        public class ThreadReply
        {
            public string Content { get; set; }
            public string Author { get; set; }
            public DateTime Date { get; set; }
        }
    }
    public class ForumThread
    {
        public const int MAX_TITLE_LENGTH = 255;
    }
    public class ThreadActions
    {
        public string GetUrl { get; set; }
        public string ReplyUrl { get; set; }
        public string DeleteUrl { get; set; }
    }
}