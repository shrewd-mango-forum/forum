﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class PostReplyRequestModel
    {
        [Required]
        public string Content { get; set; }
    }
}