﻿using System.Collections.Generic;

namespace Forum.Models
{
    public class GetUsersModel
    {
        public List<ForumUser> Users { get; set; }
    }

    public class ForumUser
    {
        public string UserName { get; set; }
    }
}