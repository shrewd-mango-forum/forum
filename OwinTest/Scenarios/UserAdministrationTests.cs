﻿using System.Net.Http;
using System.Collections.Generic;
using NUnit.Framework;

using Forum.Models;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace OwinTest
{
    [TestFixture]
    public class UserAdministrationTests
    {
        /**
         * When an anonymous user creates an account
         * Then the response is 200 OK
         */
        [Test]
        public async Task canRegisterNewUser()
        {
            string userName = "newUser";
            RegisterBindingModel bindingModel = new RegisterBindingModel { Name = userName, Password = "pass1234" };
            HttpResponseMessage registerResponse = await ForumState.Server.HttpClient.PostAsJsonAsync<RegisterBindingModel>("/api/Account/Register", bindingModel);
            Assert.AreEqual(HttpStatusCode.OK, registerResponse.StatusCode);

            HttpResponseMessage getUsersResponse = await CommonSteps.WithAdminCredential(c => c.GetAsync("/api/AccountAdmin/User"));
            Assert.AreEqual(HttpStatusCode.OK, getUsersResponse.StatusCode);
            IList<ForumUser> users = (await getUsersResponse.Content.ReadAsAsync<GetUsersModel>()).Users;
            ForumUser returnedUser = users.First(u => u.UserName.Equals(userName));
            Assert.NotNull(returnedUser);
        }

        /**
         * When an anonymous user attempts to list users
         * Then the response is 403 Forbidden
         */
        [Test]
        public async Task anonMayNotListUsers()
        {
            HttpResponseMessage anonUserResponse = await ForumState.Server.HttpClient.GetAsync("/api/AccountAdmin/User");
            Assert.AreEqual(HttpStatusCode.Unauthorized, anonUserResponse.StatusCode);
        }

        /**
         * When a user creates a new account
         * And she attempts to list users
         * Then the response is 403 Forbidden
         */
        [Test]
        public async Task plainUserMayNotListUsers() {

            TestUser user = await TestUser.NewTestUserAsync();
            HttpResponseMessage plainUserResponse = await CommonSteps.WithCredential(user.Credential, client => client.GetAsync("/api/AccountAdmin/User"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, plainUserResponse.StatusCode);
        }

        /**
         * When a user requests to become admin
         * And she lists root topics
         * Then the response contains a PostTopicUrl
         */
        [Test]
        public async Task becomeAdminByRequest()
        {
            TestUser user = await TestUser.NewTestUserAsync();
            var isAdminResponse = await CommonSteps.WithCredential(
                user.Credential, client => client.GetAsync("/api/account/checkRole/administrator"));
            var isAdminBefore = await isAdminResponse.Content.ReadAsAsync<bool>();
            Assert.AreEqual(false, isAdminBefore);
            var becomeAdminResponse = await CommonSteps.WithCredential(
                user.Credential, client => client.PostAsJsonAsync("/api/demo/trustMe", new { }));
            Assert.AreEqual(HttpStatusCode.OK, becomeAdminResponse.StatusCode);

            string adminCredential = await CommonSteps.GetLoginToken(user.Name, user.Password);
            var isAdminAfterResponse = await CommonSteps.WithCredential(adminCredential,
                client => client.GetAsync("/api/account/checkRole/administrator"));
            var isAdminAfter = await isAdminAfterResponse.Content.ReadAsAsync<bool>();
            Assert.AreEqual(true, isAdminAfter);
        }
    }
}
