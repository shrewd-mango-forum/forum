﻿using Forum.Models;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using static Forum.Models.GetTopicResponseModel;

namespace OwinTest.Scenarios
{
    [TestFixture]
    class RootTopicTests
    {
        private PostTopicRequestModel givenTopicRequest = null;
        private PostTopicResponseModel givenTopicResponse = null;
        private string postRootTopicUrl = null;
        static private int testId = 0;

        [SetUp]
        public void givenAdminCreatesTopic()
        {
            postRootTopicUrl = CommonSteps.WithAdminCredential(client => client.GetAsync(CommonSteps.ROOT_TOPIC_PATH))
                .Result.Content.ReadAsAsync<GetTopicResponseModel>()
                .Result.Actions.PostTopicUrl;
            givenTopicRequest = new PostTopicRequestModel
            {
                Id = $"root-topic-tests-{testId++}",
                Name = $"Root tests {testId}",
                Description = "Topic for RootTopicTests fixture"
            };
            HttpResponseMessage postResponse = CommonSteps
                .WithAdminCredential(client => client.PostAsJsonAsync(postRootTopicUrl, givenTopicRequest))
                .Result;
            Assert.AreEqual(HttpStatusCode.Created, postResponse.StatusCode);
            givenTopicResponse = postResponse.Content.ReadAsAsync<PostTopicResponseModel>().Result;
        }

        /**
         * When an admin lists topics (root)
         * Then the response is success
         * And the response contains the topic she posted
         * And the response shows she can create new topics
         * And the response shows she cannot create new threads
         */
        [Test]
        public async Task topicsRootAdmin()
        {
            HttpResponseMessage listResponse = await CommonSteps
                .WithAdminCredential(c => c.GetAsync(CommonSteps.ROOT_TOPIC_PATH));
            GetTopicResponseModel rootTopic = await listResponse.Content.ReadAsAsync<GetTopicResponseModel>();
            Assert.Contains(givenTopicRequest.Id, rootTopic.Topics.Select(to => to.Id).ToList());
            Assert.AreEqual(Enumerable.Empty<ThreadSummary>(), rootTopic.Threads);
            Assert.NotNull(rootTopic.Actions.PostTopicUrl);
            Assert.Null(rootTopic.Actions.PostThreadUrl);
            Assert.Null(rootTopic.Actions.DeleteTopicUrl);
            Assert.Null(rootTopic.Parent);
        }

        /**
         * When a regular user lists topics (root)
         * Then the response is success
         * And the response contains the topic that the admin created
         * And the response shows she cannot create new topics
         * And the response shows she cannot delete the topic
         */
        [Test]
        public async Task topicsRootNewUser()
        {
            TestUser testUser = await TestUser.NewTestUserAsync();
            HttpResponseMessage listResponse = await CommonSteps
                .WithCredential(testUser.Credential, c => c.GetAsync(CommonSteps.ROOT_TOPIC_PATH));
            
            GetTopicResponseModel rootTopic = await listResponse.Content.ReadAsAsync<GetTopicResponseModel>();
            Assert.Contains(givenTopicRequest.Id, rootTopic.Topics.Select(to => to.Id).ToList());
            Assert.Null(rootTopic.Actions.PostTopicUrl);
            Assert.Null(rootTopic.Actions.DeleteTopicUrl);
        }

        /**
         * When anonymous lists topics (root)
         * Then the response is success
         * And the response contains the topic that the admin created
         * And the response shows she cannot create new topics
         * And the response shows she cannot delete the topic
         */
        [Test]
        public async Task topicsRootAnonymous()
        {
            TestUser testUser = await TestUser.NewTestUserAsync();
            HttpResponseMessage listResponse = await ForumState.Server.HttpClient.GetAsync(CommonSteps.ROOT_TOPIC_PATH);

            GetTopicResponseModel rootTopic = await listResponse.Content.ReadAsAsync<GetTopicResponseModel>();
            Assert.Contains(givenTopicRequest.Id, rootTopic.Topics.Select(to => to.Id).ToList());
            Assert.Null(rootTopic.Actions.PostTopicUrl);
            Assert.Null(rootTopic.Actions.DeleteTopicUrl);
        }

        [Test]
        public async Task createTopicWithSameId()
        {
            PostTopicRequestModel duplicateTopic = new PostTopicRequestModel
            {
                Id = givenTopicRequest.Id,
                Name = $"Different name than {givenTopicRequest.Name}",
                Description = $"Also about {givenTopicRequest.Description}"
            };
            HttpResponseMessage createTopicResponse = await CommonSteps.WithAdminCredential(client => client.PostAsJsonAsync(postRootTopicUrl, duplicateTopic));
            Assert.AreEqual(HttpStatusCode.Conflict, createTopicResponse.StatusCode);
        }

        [Test]
        public async Task topicNameContainsSpecialCharacters()
        {
            PostTopicRequestModel newTopic = new PostTopicRequestModel
            {
                Id = $"{givenTopicRequest.Id}child",
                Name = "国际化",
                Description = "Topic with special characters in the name."
            };

            var newTopicHttpResponse = await CommonSteps.WithAdminCredential(client => client.PostAsJsonAsync(postRootTopicUrl, newTopic));
            Assert.AreEqual(HttpStatusCode.Created, newTopicHttpResponse.StatusCode);
            var newTopicResponse = await newTopicHttpResponse.Content.ReadAsAsync<PostTopicResponseModel>();
            
            var getTopicHttpResponse = await ForumState.Server.HttpClient.GetAsync(newTopicResponse.Actions.GetTopicUrl);
            Assert.AreEqual(HttpStatusCode.OK, getTopicHttpResponse.StatusCode);
            var getTopicResponse = await getTopicHttpResponse.Content.ReadAsAsync<GetTopicResponseModel>();
            Assert.AreEqual(newTopic.Name, getTopicResponse.Name);
        }


        [Test]
        public async Task topicIdContainsSpecialCharacters()
        {
            PostTopicRequestModel newTopic = new PostTopicRequestModel
            {
                Id = "国际化",
                Name = "国际化",
                Description = "Topic with special characters in the id."
            };

            var newTopicHttpResponse = await CommonSteps.WithAdminCredential(client => client.PostAsJsonAsync(postRootTopicUrl, newTopic));
            Assert.AreEqual(HttpStatusCode.BadRequest, newTopicHttpResponse.StatusCode);
        }
    }
}
