﻿using Forum.Models;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace OwinTest.Scenarios
{
    [TestFixture]
    class ChildTopicTests
    {
        private PostTopicRequestModel createLvl1Request;
        private PostTopicResponseModel createLvl1Response;
        private PostTopicRequestModel createLvl2Request;
        private PostTopicResponseModel createLvl2Response;
        static private int testId = 0;

        [SetUp]
        public void givenAdminCreatesTopic()
        {
            testId++;

            createLvl1Request = new PostTopicRequestModel
            {
                Id = $"child-topic-tests-{testId}",
                Name = $"Parent Topic {testId}",
                Description = "First parent topic"
            };
            string newTopicPath = CommonSteps.WithAdminCredential(client => client.GetAsync(CommonSteps.ROOT_TOPIC_PATH)).Result
                .Content.ReadAsAsync<GetTopicResponseModel>().Result.Actions.PostTopicUrl;
            HttpResponseMessage postParentTopicResponse = CommonSteps
                .WithAdminCredential(client => client.PostAsJsonAsync(newTopicPath, createLvl1Request))
                .Result;
            Assert.AreEqual(HttpStatusCode.Created, postParentTopicResponse.StatusCode);
            createLvl1Response = postParentTopicResponse.Content.ReadAsAsync<PostTopicResponseModel>().Result;

            createLvl2Request = new PostTopicRequestModel
            {
                Id = $"{createLvl1Request.Id}.{testId}",
                Name = $"Child Topic {testId}",
                Description = "Child of first parent topic"
            };
            HttpResponseMessage postChildTopicResponse = CommonSteps
                .WithAdminCredential(client => client.PostAsJsonAsync(createLvl1Response.Actions.PostTopicUrl, createLvl2Request))
                .Result;
            Assert.AreEqual(HttpStatusCode.Created, postChildTopicResponse.StatusCode);
            createLvl2Response = postChildTopicResponse.Content.ReadAsAsync<PostTopicResponseModel>().Result;
        }

        /**
         * When an admin lists topics (root)
         * Then the response is success
         * And the response contains the level1 topic
         * And the response does not contain the level2 topic
         */
        [Test]
        public async Task listRootTopic()
        {
            HttpResponseMessage listRootResponse = await CommonSteps
                .WithAdminCredential(c => c.GetAsync(CommonSteps.ROOT_TOPIC_PATH));
            GetTopicResponseModel rootTopic = await listRootResponse.Content.ReadAsAsync<GetTopicResponseModel>();
            List<string> idsUnderRoot = rootTopic.Topics.Select(to => to.Id).ToList();
            CollectionAssert.Contains(idsUnderRoot, createLvl1Request.Id);
            CollectionAssert.DoesNotContain(idsUnderRoot, createLvl2Request.Id);
        }

        /**
         * When an admin lists topics (level1)
         * Then the response is success
         * And the response does not contain the level1
         * And the response does contains the level2 topic
         * And the response's parentTopic is the root topic
         */
        [Test]
        public async Task listLvl1Topic() {
            HttpResponseMessage getLvl1Response = await CommonSteps
                .WithAdminCredential(c => c.GetAsync(createLvl1Response.Actions.GetTopicUrl));
            GetTopicResponseModel lvl1Topic = await getLvl1Response.Content.ReadAsAsync<GetTopicResponseModel>();

            Assert.AreEqual(createLvl1Request.Id, lvl1Topic.Id);
            Assert.AreEqual(createLvl1Request.Name, lvl1Topic.Name);
            Assert.AreEqual(createLvl1Request.Description, lvl1Topic.Description);

            List<string> idsUnderLvl1 = lvl1Topic.Topics.Select(to => to.Id).ToList();
            CollectionAssert.DoesNotContain(idsUnderLvl1, createLvl1Request.Id);
            CollectionAssert.Contains(idsUnderLvl1, createLvl2Request.Id);
            HttpResponseMessage listRootResponse = await CommonSteps
                .WithAdminCredential(c => c.GetAsync(CommonSteps.ROOT_TOPIC_PATH));
            GetTopicResponseModel rootTopic = await listRootResponse.Content.ReadAsAsync<GetTopicResponseModel>();
            Assert.AreEqual(rootTopic.Actions.GetTopicUrl, lvl1Topic.Parent.GetTopicUrl);
            Assert.IsNull(lvl1Topic.Parent.Id);
            Assert.IsNull(lvl1Topic.Parent.Description);
        }

        /**
         * When an admin creates a level3 topic
         * Then the response is success
         * When the admin lists the level2 topic
         * Then the child's child is the level3 topic
         * When the admin lists the level3 topic
         * Then the grandchild's parent is the level2 topic
         */
        [Test]
        public async Task createLevel3()
        {
            PostTopicRequestModel level3Topic = new PostTopicRequestModel
            {
                Id = $"{createLvl2Request.Id}.{testId}",
                Name = $"Grandchild  ${testId}",
                Description = "Child of child of root topic"
            };
            HttpResponseMessage createLvl3TopicResponse = await CommonSteps
                .WithAdminCredential(client => client.PostAsJsonAsync(createLvl2Response.Actions.PostTopicUrl, level3Topic));
            Assert.AreEqual(HttpStatusCode.Created, createLvl3TopicResponse.StatusCode);

            HttpResponseMessage listLvl2Request = await CommonSteps
                .WithAdminCredential(c => c.GetAsync(createLvl2Response.Actions.GetTopicUrl));
            var getChild = await listLvl2Request.Content.ReadAsAsync<GetTopicResponseModel>();
            List<string> idsUnderChild = getChild
                .Topics
                .Select(topic => topic.Id)
                .ToList();
            CollectionAssert.AreEqual(new List<string> { level3Topic.Id }, idsUnderChild);

            HttpResponseMessage getLvl3Response = await CommonSteps
                .WithAdminCredential(c => c.GetAsync(getChild.Topics.First().GetTopicUrl));
            GetTopicResponseModel gottenTopic = await getLvl3Response.Content.ReadAsAsync<GetTopicResponseModel>();
            Assert.AreEqual(level3Topic.Id, gottenTopic.Id);
            Assert.AreEqual(level3Topic.Name, gottenTopic.Name);
            Assert.AreEqual(level3Topic.Description, gottenTopic.Description);

            var grandchildsParent = gottenTopic.Parent;
            Assert.AreEqual(createLvl2Request.Id, grandchildsParent.Id);
            Assert.AreEqual(createLvl2Response.Actions.GetTopicUrl, grandchildsParent.GetTopicUrl);
        }



        /**
         * When an admin deletes lvl2 topic
         * Then getting that topic returns 404 Not Found
         * And getting the lvl1 topic does not contain the deleted topic
         */
        [Test]
        public async Task adminCanDeleteEmptyTopic()
        {
            Assert.NotNull(createLvl2Response.Actions.DeleteTopicUrl);
            var deleteResponse = await CommonSteps.WithAdminCredential(
                client => client.DeleteAsync(createLvl2Response.Actions.DeleteTopicUrl));
            Assert.AreEqual(HttpStatusCode.OK, deleteResponse.StatusCode);

            var getAfterDeleteResponse = await CommonSteps.WithAdminCredential(
                client => client.GetAsync(createLvl2Response.Actions.GetTopicUrl));
            Assert.AreEqual(HttpStatusCode.NotFound, getAfterDeleteResponse.StatusCode);

            var getLvl1 = await CommonSteps.WithAdminCredential(
                client => client.GetAsync(createLvl1Response.Actions.GetTopicUrl));
            var parentAfterDelete = await getLvl1.Content.ReadAsAsync<GetTopicResponseModel>();
            CollectionAssert.DoesNotContain(
                parentAfterDelete.Topics.Select(t => t.Id), createLvl2Request.Id);
        }

        /**
         * Given a thread is created under the lv2 topic
         * When an admin deletes the lv2 topic
         * Then getting the thread returns 404 Not Found
         */
        [Test]
        public async Task adminDeletesTopicContainingThread()
        {
            //given
            var testUser = await TestUser.NewTestUserAsync();
            var postThreadResponse = await CommonSteps.WithCredential(testUser.Credential,
                client => client.PostAsJsonAsync(createLvl2Response.Actions.PostThreadUrl,
                new PostThreadRequestModel
                {
                    ParentTopicId = createLvl2Request.Id,
                    Title = "This is my favorite topic.",
                    Content = "I hope it lasts forever."
                }));
            Assert.AreEqual(HttpStatusCode.Created, postThreadResponse.StatusCode);
            var postedThread = await postThreadResponse.Content.ReadAsAsync<PostThreadResponseModel>();

            //when
            await CommonSteps.WithAdminCredential(client => client.DeleteAsync(createLvl2Response.Actions.DeleteTopicUrl));

            //then
            var getThreadAfterDeleteResponse = await CommonSteps.WithCredential(testUser.Credential,
                client => client.GetAsync((postedThread).Actions.GetUrl));
            Assert.AreEqual(HttpStatusCode.NotFound, getThreadAfterDeleteResponse.StatusCode);
        }


        /**
         * When an admin deletes a topic containing another topic
         * Then getting the bottom topic returns a 404 Not Found
         */
        [Test]
        public async Task adminDeletesParentTopic()
        {
            //when
            var deleteParentResponse = await CommonSteps.WithAdminCredential(
                client => client.DeleteAsync(createLvl1Response.Actions.DeleteTopicUrl));
            Assert.AreEqual(HttpStatusCode.OK, deleteParentResponse.StatusCode);

            //then
            var getAfterDeleteResponse = await CommonSteps.WithAdminCredential(
                client => client.GetAsync(createLvl2Response.Actions.GetTopicUrl));
            Assert.AreEqual(HttpStatusCode.NotFound, getAfterDeleteResponse.StatusCode);
        }

        /**
         * When a non-admin user attempts to delete the topic
         * Then the response indicates the user is not authorized
         */
        [Test]
        public async Task userGuessesDeleteTopicUrl()
        {
            var testUser = await TestUser.NewTestUserAsync();
            var deleteTopicResponse = await CommonSteps.WithCredential(testUser.Credential,
                client => client.DeleteAsync(createLvl2Response.Actions.DeleteTopicUrl));
            Assert.AreEqual(HttpStatusCode.Unauthorized, deleteTopicResponse.StatusCode);
        }
    }
}
