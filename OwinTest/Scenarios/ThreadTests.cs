﻿using Forum.Models;
using NUnit.Framework;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using static Forum.Models.GetTopicResponseModel;

namespace OwinTest.Scenarios
{
    [TestFixture]
    public class ThreadTests
    {
        private static int topicCount = 0;
        private PostTopicRequestModel givenTopicRequest;
        private PostTopicResponseModel givenTopicResponse;
        [SetUp]
        public void givenAdminCreatesTopic()
        {
            topicCount++;
            givenTopicRequest = new PostTopicRequestModel {
                Id = $"thread-tests-{topicCount++}",
                Name = $"Thread Tests {topicCount}",
                Description = "Some topic",
            };
            var newTopicPath = CommonSteps
                .WithAdminCredential(client => client.GetAsync(CommonSteps.ROOT_TOPIC_PATH)).Result
                .Content.ReadAsAsync<GetTopicResponseModel>().Result.Actions.PostTopicUrl;
            HttpResponseMessage postResponse = CommonSteps
                .WithAdminCredential(client => client.PostAsJsonAsync(newTopicPath, givenTopicRequest))
                .Result;
            Assert.AreEqual(HttpStatusCode.Created, postResponse.StatusCode);
            givenTopicResponse = postResponse.Content.ReadAsAsync<PostTopicResponseModel>().Result;
        }

        /**
         * When a user gets the new topic
         * Then the response is OK
         * And the topic in the response shows she may create new threads
         * And the topic in the response shows she may not create new topics
         */
        [Test]
        public async Task userGetsTopic()
        {
            TestUser testUser = await TestUser.NewTestUserAsync();
            HttpResponseMessage response = await CommonSteps.WithCredential(testUser.Credential,
                c => c.GetAsync(givenTopicResponse.Actions.GetTopicUrl));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            GetTopicResponseModel getTopicResponse = await response.Content.ReadAsAsync<GetTopicResponseModel>();
            Assert.NotNull(getTopicResponse.Actions.PostThreadUrl);
            Assert.Null(getTopicResponse.Actions.PostTopicUrl);
        }

        /**
         * When anonymous gets the new topic
         * Then the response is OK
         * And the topic in the response shows she may not create new topics
         * And the topic in the response shows she may not create new threads
         */
        [Test]
        public async Task anonGetsTopic()
        {
            HttpResponseMessage response = await ForumState.Server.HttpClient
                .GetAsync(givenTopicResponse.Actions.GetTopicUrl);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            GetTopicResponseModel topicResponse = await response.Content.ReadAsAsync<GetTopicResponseModel>();
            Assert.Null(topicResponse.Actions.PostTopicUrl);
            Assert.Null(topicResponse.Actions.PostThreadUrl);
        }

        /**
         * When a user creates a thread under the new topic
         * Then the response is Successfully Created
         * And the response contains a valid url to get the thread
         */
        [Test]
        public async Task userCanPostThreadUnderTopic()
        {
            //user creates thread in topic
            TestUser testUser = await TestUser.NewTestUserAsync();
            PostThreadRequestModel post = new PostThreadRequestModel {
                Content = "Hi",
                Title = "My New Thread",
                ParentTopicId = givenTopicRequest.Id
            };
            HttpResponseMessage postThreadHttpResponse = await CommonSteps.WithCredential(
                testUser.Credential,
                client => client.PostAsJsonAsync(givenTopicResponse.Actions.PostThreadUrl, post));
            Assert.AreEqual(HttpStatusCode.Created, postThreadHttpResponse.StatusCode);
            PostThreadResponseModel postThreadResponse = await postThreadHttpResponse.Content.ReadAsAsync<PostThreadResponseModel>();

            //user gets thread directly
            HttpResponseMessage getThreadHttpResponse = await CommonSteps.WithCredential(testUser.Credential,
                client => client.GetAsync(postThreadResponse.Actions.GetUrl));
            Assert.AreEqual(HttpStatusCode.OK, getThreadHttpResponse.StatusCode);
            GetThreadResponseModel getThreadResponse = await getThreadHttpResponse.Content.ReadAsAsync<GetThreadResponseModel>();
            Assert.AreEqual(post.Title, getThreadResponse.Title);
            Assert.AreEqual(postThreadResponse.Id, getThreadResponse.Id);
        }

        /**
         * Given a user creates a thread in the new topic
         * When an anonymous user gets the topic
         * Then the response contains the thread
         * And the anonymous user can get the thread
         */
        [Test]
        public async Task anonCanGetThreadByBrowsingTopic()
        {
            //user creates thread in topic
            TestUser testUser = await TestUser.NewTestUserAsync();
            PostThreadRequestModel post = new PostThreadRequestModel {
                Content = "Hi",
                Title = "My New Thread",
                ParentTopicId = givenTopicRequest.Id
            };
            HttpResponseMessage postThreadHttpResponse = await CommonSteps.WithCredential(
                testUser.Credential,
                client => client.PostAsJsonAsync(givenTopicResponse.Actions.PostThreadUrl, post));
            Assert.AreEqual(HttpStatusCode.Created, postThreadHttpResponse.StatusCode);

            //anon gets topic and sees thread preview
            var listRootTopicHttpResponse = await ForumState.Server.HttpClient.GetAsync(givenTopicResponse.Actions.GetTopicUrl);
            var listRootTopicResponse = await listRootTopicHttpResponse.Content.ReadAsAsync<GetTopicResponseModel>();
            var newThreadSummary = await getThreadSummary(givenTopicResponse.Actions.GetTopicUrl, post.Title);
            Assert.NotNull(newThreadSummary);

            //anon gets thread
            var getThreadHttpResponse = await ForumState.Server.HttpClient.GetAsync(newThreadSummary.ThreadUrl);
            GetThreadResponseModel getThreadResponse = await getThreadHttpResponse.Content.ReadAsAsync<GetThreadResponseModel>();
            Assert.AreEqual(post.Title, getThreadResponse.Title);
            Assert.AreEqual(newThreadSummary.Id, getThreadResponse.Id);
            Assert.AreEqual(testUser.Name, getThreadResponse.Author);
            Assert.AreEqual(givenTopicResponse.Actions.GetTopicUrl, getThreadResponse.Topic.GetTopicUrl);
            Assert.AreEqual(givenTopicRequest.Id, getThreadResponse.Topic.Id);
            Assert.Null(getThreadResponse.Actions.ReplyUrl);
        }

        /**
         * Given a user creates a thread in the topic
         * When a different user replies to that thread
         * Then an anonymous user can see two replies in that thread
         */
        [Test]
        public async Task authnUserCanReplyToThread()
        {
            //first user creates thread in topic
            TestUser createThreadUser = await TestUser.NewTestUserAsync();
            PostThreadRequestModel postThread = new PostThreadRequestModel {
                Content = "Hi",
                Title = "My New Thread",
                ParentTopicId = givenTopicRequest.Id
            };
            HttpResponseMessage postThreadResponse = await CommonSteps.WithCredential(
                createThreadUser.Credential,
                client => client.PostAsJsonAsync(givenTopicResponse.Actions.PostThreadUrl, postThread));
            Assert.AreEqual(HttpStatusCode.Created, postThreadResponse.StatusCode);

            //second user gets thread and replies to it
            var threadSummary = await getThreadSummary(givenTopicResponse.Actions.GetTopicUrl, postThread.Title);
            TestUser responseUser = await TestUser.NewTestUserAsync();
            var getThreadHttpResponse = await CommonSteps.WithCredential(responseUser.Credential,
                client => client.GetAsync(threadSummary.ThreadUrl));
            var replyUrl = (await getThreadHttpResponse.Content.ReadAsAsync<GetThreadResponseModel>()).Actions.ReplyUrl;
            Assert.IsNotEmpty(replyUrl);
            PostReplyRequestModel replyRequest = new PostReplyRequestModel { Content = "I disagree." };
            HttpResponseMessage replyResponse = await CommonSteps.WithCredential(responseUser.Credential,
                c => c.PostAsJsonAsync(replyUrl, replyRequest));
            Assert.AreEqual(HttpStatusCode.Created, replyResponse.StatusCode);
            Assert.AreEqual(postThreadResponse.Headers.GetValues("Location"), replyResponse.Headers.GetValues("Location"));

            //anon gets thread (directly)
            HttpResponseMessage getThreadResponse = await ForumState.Server.HttpClient.GetAsync(threadSummary.ThreadUrl);
            GetThreadResponseModel getThread = await getThreadResponse.Content.ReadAsAsync<GetThreadResponseModel>();
            GetThreadResponseModel.ThreadReply originalPost = getThread.Replies[0];
            Assert.AreEqual(createThreadUser.Name, originalPost.Author);
            Assert.AreEqual(postThread.Content, originalPost.Content);
            GetThreadResponseModel.ThreadReply secondPost = getThread.Replies[1];
            Assert.AreEqual(responseUser.Name, secondPost.Author);
            Assert.AreEqual(replyRequest.Content, secondPost.Content);
        }

        /**
         * Given a user creates a thread in the topic
         * When an anonymous user attempts to reply to that thread
         * The anonymous user receives 403 Unauthorized
         */
        [Test]
        public async Task anonMayNotReply()
        {
            //given
            var testUser = await TestUser.NewTestUserAsync();
            var post = new PostThreadRequestModel {
                Content = "Hi",
                Title = "My New Thread",
                ParentTopicId = givenTopicRequest.Id
            };
            var postThreadResponse = await CommonSteps.WithCredential(
                testUser.Credential,
                client => client.PostAsJsonAsync(givenTopicResponse.Actions.PostThreadUrl, post));
            Assert.AreEqual(HttpStatusCode.Created, postThreadResponse.StatusCode);
            var replyUrl = (await postThreadResponse.Content.ReadAsAsync<PostThreadResponseModel>()).Actions.ReplyUrl;

            //when
            var reply = new PostReplyRequestModel { Content = "kill you're self" };
            var postReplyResponse = await ForumState.Server.HttpClient.PostAsJsonAsync(replyUrl, reply);
            Assert.AreEqual(HttpStatusCode.Unauthorized, postReplyResponse.StatusCode);
        }

        private async Task<ThreadSummary> getThreadSummary(string topicUrl, string threadTitle)
        {
            var listTopicHttpResponse = await ForumState.Server.HttpClient.GetAsync(topicUrl);
            var listTopicResponse = await listTopicHttpResponse.Content.ReadAsAsync<GetTopicResponseModel>();
            return listTopicResponse.Threads.Where(th => th.Title.Equals(threadTitle)).SingleOrDefault();
        }
    }
}
