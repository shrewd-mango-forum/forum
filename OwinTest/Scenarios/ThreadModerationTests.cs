﻿using Forum.Models;
using NUnit.Framework;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace OwinTest.Scenarios
{
    class ThreadModerationTests
    {
        private static int topicCount = 0;
        private PostTopicResponseModel givenTopic = null;
        private TestUser originalPoster = null;
        private PostThreadResponseModel givenThread = null;

        /**
         * Given an admin creates a topic
         * And a user creates a thread under that topic
         */
        [SetUp]
        public void givenTopicAndThread()
        {
            topicCount++;
            string topicId = $"moderation-tests-{topicCount++}";
            string topicDescription = "Some topic";
            var newTopicRequest = new PostTopicRequestModel {
                Id = topicId,
                Name = $"Thread Moderation Tests {topicCount++}",
                Description = topicDescription
            };
            string newTopicPath = CommonSteps
                .WithAdminCredential(client => client.GetAsync(CommonSteps.ROOT_TOPIC_PATH)).Result
                .Content.ReadAsAsync<GetTopicResponseModel>().Result.Actions.PostTopicUrl;
            HttpResponseMessage postTopicResponse = CommonSteps
                .WithAdminCredential(client => client.PostAsJsonAsync(newTopicPath, newTopicRequest))
                .Result;
            Assert.AreEqual(HttpStatusCode.Created, postTopicResponse.StatusCode);

            givenTopic = postTopicResponse.Content.ReadAsAsync<PostTopicResponseModel>().Result;

            originalPoster = TestUser.NewTestUserAsync().Result;
            var newThread = new PostThreadRequestModel() {
                ParentTopicId = topicId,
                Title = "Troll-free zone",
                Content = "Please don't troll this thread."
            };
            HttpResponseMessage postThreadResponse = CommonSteps.WithCredential(originalPoster.Credential,
                    client => client.PostAsJsonAsync(givenTopic.Actions.PostThreadUrl, newThread)).Result;
            Assert.AreEqual(HttpStatusCode.Created, postThreadResponse.StatusCode);
            givenThread = postThreadResponse.Content.ReadAsAsync<PostThreadResponseModel>().Result;
        }

        /**
         * When an admin deletes the thread
         * Then the response is OK
         * And a user who gets the topic does not see the thread listed
         * And a user cannot get the thread directly
         */
        [Test]
        public async Task adminCanDeleteThread()
        {
            var getThreadResponse = await CommonSteps.WithAdminCredential(client => client.GetAsync(givenThread.Actions.GetUrl));
            var thread = await getThreadResponse.Content.ReadAsAsync<GetThreadResponseModel>();
            var deleteThreadResponse = await CommonSteps.WithAdminCredential(client => client.DeleteAsync(thread.Actions.DeleteUrl));
            Assert.AreEqual(HttpStatusCode.OK, deleteThreadResponse.StatusCode);

            var getTopicResponse = await CommonSteps.WithCredential(originalPoster.Credential, client =>
                client.GetAsync(givenTopic.Actions.GetTopicUrl));
            var topic = await getTopicResponse.Content.ReadAsAsync<GetTopicResponseModel>();
            CollectionAssert.DoesNotContain(
                topic.Threads.Select(t => t.Title),
                thread.Title);
            var getDeletedThreadResponse = await CommonSteps.WithCredential(originalPoster.Credential, client =>
                client.GetAsync(givenThread.Actions.GetUrl));
            Assert.AreEqual(HttpStatusCode.NotFound, getDeletedThreadResponse.StatusCode);
        }

        /**
         * When the original poster gets the thread
         * Then the response shows they cannot delete the thread
         */
        [Test]
        public async Task opDoesNotSeeDeleteThreadUrl()
        {
            var getThreadResponse = await CommonSteps.WithCredential(originalPoster.Credential,
                client => client.GetAsync(givenThread.Actions.GetUrl));
            var thread = await getThreadResponse.Content.ReadAsAsync<GetThreadResponseModel>();
            Assert.IsNull(thread.Actions.DeleteUrl);
        }

        /**
         * When the original poster guesses the 'delete thread' url
         * Then the response shows he is forbidden to do so
         * And a user can still get the thread
         */
        [Test]
        public async Task opMayNotDeleteThread()
        {
            var getThreadAsAdminResponse = await CommonSteps.WithAdminCredential(
                client => client.GetAsync(givenThread.Actions.GetUrl));
            var thread = await getThreadAsAdminResponse.Content.ReadAsAsync<GetThreadResponseModel>();
            var deleteThreadResponse = await CommonSteps.WithCredential(originalPoster.Credential,
                client => client.DeleteAsync(givenThread.Actions.DeleteUrl));

            Assert.AreEqual(HttpStatusCode.MethodNotAllowed, deleteThreadResponse.StatusCode);
            var getThreadAfterDeletionResponse =
                await ForumState.Server.HttpClient.GetAsync(givenThread.Actions.GetUrl);
            Assert.AreEqual(HttpStatusCode.OK, getThreadAfterDeletionResponse.StatusCode);
            var threadAfterDeletion = await getThreadAfterDeletionResponse.Content.ReadAsAsync<GetThreadResponseModel>();
            Assert.AreEqual(thread.Title, threadAfterDeletion.Title);
        }
    }
}
