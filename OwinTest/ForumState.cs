﻿using Forum.Models;
using Microsoft.Owin.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OwinTest
{
    class ForumState
    {
        public static String AdminToken { get; set; }
        public static TestServer Server { get; set; }
    }
}
