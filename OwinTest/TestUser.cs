﻿using Forum.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OwinTest
{
    class TestUser
    {
        public string Name { get; private set; }
        public string Password { get; private set; }
        public string Credential { get; private set; }
        private static int count = 0;
        public static async Task<TestUser> NewTestUserAsync()
        {
            count++;
            string name = "TestUser" + count;
            string password = "Password1234";
            RegisterBindingModel bindingModel = new RegisterBindingModel { Name = name, Password = password };
            HttpResponseMessage registerResponse = await ForumState.Server.HttpClient.PostAsJsonAsync<RegisterBindingModel>("/api/Account/Register", bindingModel);
            Assert.AreEqual(HttpStatusCode.OK, registerResponse.StatusCode);
            String credential = await CommonSteps.GetLoginToken(name, password);
            return new TestUser { Name = name, Password = password, Credential = credential };
        }
    }
}
