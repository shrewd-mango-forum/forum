﻿using Forum.Models;
using Microsoft.Owin.Testing;
using NUnit.Framework;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace OwinTest
{
    [SetUpFixture]
    class SetUpFixture
    {
        [OneTimeSetUp]
        public void SetUp()
        {
            ForumState.Server = TestServer.Create<Forum.Startup>();
            SetAdminTokenAsync().Wait();
        }
        [OneTimeTearDown]
        public void TearDown()
        {
            DeleteAllUsersAsync().Wait();
            ForumState.Server.Dispose();
        }

        private async Task SetAdminTokenAsync()
        {
            ForumState.AdminToken = await CommonSteps.GetLoginToken("Admin", "MySecret");
        }

        private async Task DeleteAllUsersAsync()
        {
            var getAllResponse = await CommonSteps.WithAdminCredential(client => client.GetAsync("/api/AccountAdmin/User"));
            var users = (await getAllResponse.Content.ReadAsAsync<GetUsersModel>()).Users;
            await Task.WhenAll(users
                .Where(u => !u.UserName.Equals("Admin"))
                .Select(u => CommonSteps.WithAdminCredential(async client => await client.DeleteAsync("/api/AccountAdmin/User/" + u.UserName))));
        }
    }
}
