﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace OwinTest
{
    class CommonSteps
    {
        public const string ROOT_TOPIC_PATH = "";
        public static async Task<String> GetLoginToken(String userName, String password)
        {
            var content = new FormUrlEncodedContent(new[] 
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", userName),
                new KeyValuePair<string, string>("password", password)
            });

            HttpResponseMessage response = await ForumState.Server.HttpClient.PostAsync("/Token", content);
            Assert.IsTrue(response.IsSuccessStatusCode);
            string responseContent = await response.Content.ReadAsStringAsync();
            JObject jObj = JObject.Parse(responseContent);
            return jObj["access_token"].ToString();
        }

        public static Task<HttpResponseMessage> WithAdminCredential(Func<HttpClient, Task<HttpResponseMessage>> action)
        {
            return WithCredential(ForumState.AdminToken, action);
        }

        public static Task<HttpResponseMessage> WithCredential(String token, Func<HttpClient, Task<HttpResponseMessage>> action)
        {
            HttpClient client = new HttpClient(new AddHeaderHttpHandler("Authorization", "Bearer " + token));
            client.BaseAddress = ForumState.Server.HttpClient.BaseAddress;
            return action(client);
        }

        private class AddHeaderHttpHandler : DelegatingHandler
        {
            private readonly string headerKey;
            private readonly string headerValue;
            public AddHeaderHttpHandler(string headerKey, string headerValue)
                : base(ForumState.Server.Handler)
            {
                this.headerKey = headerKey;
                this.headerValue = headerValue;
            }

            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken token)
            {
                request.Headers.Add(headerKey, headerValue);
                return base.SendAsync(request, token);
            }
        }
    }
}
