This is a forums backend that I wrote to get familiar with ASP.NET Web API. It
allows an Administrator to create topics and subtopics, and for registered
users to create and reply to threads within those topics. Data is persisted to
SQL Server.

The solution is organized into 3 projects:

- Forum is the implementation. It can be run or deployed directly from Visual Studio.
- ForumModels contains classes which model the interface.
- OwinTest contains a suite of functional tests.
